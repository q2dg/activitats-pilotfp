# Client - Servidor

![](logo.png)

## Objectius

Treballar el potencial d'IsardVDI per a pràctiques de xarxes personals simulant entorns reals de manera fàcil i ràpida.

## Continguts

- Descripció del laboratori
- Creació i connexió dels escriptoris client i servidor
- Comprovacions de connectivitat
- Instal.lació i comprovació del servidor
- Connexió al servidor localment

## Metodologia

El curs es fa a l’aula virtual pilot [https://pilotfp.gencat.isardvdi.com](https://pilotfp.gencat.isardvdi.com). La metodologia es basa en el fet que l’alumne (professorat inscrit) és el centre de l’aprenentatge. Aquest aprenentatge es construirà a partir de la pràctica proposada. Assolint les competències relacionades amb les metodologies actives mitjançant el feedback.

## Avaluació

S'avaluarà la realització de l'enquesta sobre els continguts d'aquest exercici.

## Desenvolupament

### Descripció del laboratori

En aquesta activitat dins d'IsardVDI crearem dos escriptoris, `client` i `servidor` i els configurarem amb la `xarxa personal`. Els escriptoris poden ser basats en la mateixa plantilla d'`Ubuntu Desktop`, tot i que usarem un com a servidor i l'altre com a client.

A IsardVDI hi ha diferents tipus de xarxes. Entre aquestes hi ha la xarxa `Personal`, que interconnecta tots els escriptoris d'un usuari per tal de poder realitzar connexions de xarxa entre ells.

``` mermaid
graph LR
  pb1(Servidor):::tp --> eb1(client):::dk
  classDef tp stroke:#ff3465,fill:#ffd1dc,stroke-width:2px
  classDef dk stroke:#30d200,fill:#cdffbe,stroke-width:1px
```

### Creació i connexió dels escriptoris client i servidor

Primer crearem els dos escriptoris a partir d'una plantilla `Ubuntu Desktop`:

![](plantilla.png)

1. Crear escriptori de nom `client`
2. Crear escriptori de nom `servidor`

Una vegada els tinguem creats els editarem i farem que tinguin dues interfícies de xarxa, la `default` que els proporciona connexió a Internet i afegirem una nova, la xarxa de nom `Personal`:

1. Anar a l'apartat d'interfícies dins l'edició de l'escriptori. Veurem que ja té la xarxa `default`.
2. Afegir la xarxa `Personal` i acceptar els canvis.

![](xarxa_personal.png)

### Comprovacions de connectivitat

Amb els dos escriptoris editats amb la nova xarxa afegida, els iniciarem i obrirem un terminal (podem buscar l'aplicació de terminal clicant a l'icona d'Ubuntu a dalt a l'esquerra).

Comprovem als dos escriptoris, client i servidor, que ens pareixen tres interfícies amb l'ordre `ip address show`:

![](ipas.png)

- **lo**: Interfície virtual de `loopback` amb adreça IP 127.0.0.1 que utilitzen els sistemes operatius per a connexions locals amb ells mateixos. No ens interessa en aquesta activitat.
- **enp1s0**: Aquesta xarxa és la `default` que permet connectivitat amb Internet i que ja té adreçament IP assignat dinàmicament.
- **enp2s0**: Aquesta xarxa és la `personal` que hem afegit als escriptoris.

#### Xarxa default

Una d'aquestes tindrà una adreça de xarxa tipus 192.168.X.Y que se li ha assignat dinàmicament en iniciar (DHCP). Aquesta sabrem que és la xarxa `default` que ja portava l'escriptori quan l'hem creat. És la que proporciona connectivitat a Internet mitjançant resolució de noms (DNS) i una porta d'enllaç cap a un encaminador (GATEWAY).

Podem comprovar que aquesta xarxa `default` no permet la connectivitat entre els escriptoris, només la sortida cap a Internet, si fem un `ping` des de l'escriptori client cap a l'adreça de l'escriptori servidor, i a l'inrevés.

#### Xarxa personal

L'altra interfície no tindrà assignada cap adreça de xarxa, per tan l'identificarem com la xarxa `Personal`. Procedirem a assignar un adreçament de xarxa a cada escriptori en aquesta interfície i a comprovar que tenen connectivitat entre ells.

Els nous Ubuntu porten la utilitat [netplan](https://netplan.io) per a configurar els serveis de xarxa, ja sigui amb el servei *NetworkManager* o bé amb *networkd*.

Es tracta de configuracions de xarxa en format `yaml` que es troben a `/etc/netplan`. 

Per a saber si el nostre Ubuntu està fent servir *netplan* o bé directament els sistemes de configuració via *NetworkManager* o *networkd* mirarem el contingut del fitxer que trobem ja a /etc/netplan/...

![](netplan.png)

A la imatge podem veure que no s'està fent servir el *netplan* per a configurar les xarxes, es fa servir la configuració amb *NetworkManager*.

##### Configuració amb Network Manager

!!! info "Configuració de xarxa"

    Només realitzar la configuració amb un dels dos sistemes (NetworkManager o bé Netplan)

En cas que el gestor de xarxes del nostre Ubuntu sigui el Network Manager i no ho volguem canviar, utilitzarem la utilitat `nmcli` per a configurar l'adreçament estàtic. Podem veure amb `ǹmcli connection show` quines configuracions ha establert el Network Manager per a les interfícies que ha trobat en iniciar:

![](nmcli.png)

Amb aquesta ordre configurem l'adreça de l'escriptori client:

```
sudo nmcli con mod Wired\ connection\ 2 ipv4.addresses 10.10.10.1/24
```

Si tot és correcte, ara hauríem de poder fer `ping` entre les dues màquines:

  Des del client: `ping 10.10.10.2`
  Des del servidor: `ping 10.10.10.1`

En cas contrari caldrà revisar:

- Que tenim la interfície `Personal` assignada als dos escriptoris
- Que hem identificat correctament la interfície dins l'escriptori (la que no tenia adreçament IP inicialment)
- Que hem posat l'adreça 10.10.10.2/24 a l'escriptori client i la 10.10.10.1/24 a l'escriptori servidor
- Que no hi ha errors en aplicar el netplan (`sudo netplan apply`)

També podem configurar les xarxes des del mode gràfic. Teniu informació ampliada de configuracions a [https://ioc.xtec.cat/materials/FP/Recursos/fp_asx_m01_/web/fp_asx_m01_htmlindex/WebContent/u3/a2/continguts.html](https://ioc.xtec.cat/materials/FP/Recursos/fp_asx_m01_/web/fp_asx_m01_htmlindex/WebContent/u3/a2/continguts.html)

##### Configuració amb Netplan

!!! info "Configuració de xarxa"

    Només realitzar la configuració amb un dels dos sistemes (NetworkManager o bé Netplan)

El **netplan** es configura a `/etc/netplan/config.yaml` (o un fitxer yaml qualsevol). Si obrim aquest fitxer veurem que porta configurada amb assignament IP dinàmic per a la interfície que hem identificat com a `default`:

```
network:
  version: 2
  ethernets:
    enp1s0:
      dhcp4: true
```

El que farem nosaltres és afegir la configuració al final de l'existent per a aquesta nova xarxa `Personal` amb adreçament estàtic:

!!! info "Edició de fitxers de sistema"

    Cal editar aquests fitxers de sistema (`/etc/netplan/config.yaml`) amb permisos suficients. Per a fer-ho usarem l'editor que preferim (vi, vim, nano, ...) amb `sudo` davant per tal d'elevar els nostres permisos a superusuari.
    `sudo nano /etc/netplan/config.yaml`

- Escriptori client

```
    enp2s0:
      addresses:
        - 10.10.10.2/24
```

- Escriptori servidor

```
    enp2s0:
      addresses:
        - 10.10.10.1/24
```

Una vegada hàgim afegit això al final del fitxer, comprovarem que la configuració és correcta amb `sudo netplan try`. Si hi ha errors haurem de revisar la documentació de netplan a partir dels missatges d'error que ens indiquin per tal de solucionar-ho.

Aplicarem la configuració amb `sudo netplan apply` i comprovarem que el nou adreçament s'ha assignat a la interfície amb `ip address show`.

## Instal.lació i configuració del servidor

Per a acabar l'activitat instal.larem un servidor web `nginx` a l'escriptori que fa de servidor. Des del terminal del servidor executarem:

```
sudo apt install nginx -y
```

Una vegada instal·lat al servidor, anirem a l'escriptori client i obrirem el navegador web. A l'adreça web hi posarem la IP del servidor: 10.10.10.1 i comprovarem que hi podem arribar i veiem una plana com aquesta:

![](nginx.png)

Pot ser que no aparegui. Això és degut al fet que, per defecte, els sistemes operatius Ubuntu porten activat un tallafoc, el `ufw` que caldrà desactivar a l'escriptori servidor per a comprovar la connectivitat web (port 80 http):

```
sudo systemctl stop ufw
```
NOTA: Si reiniciem l'escriptori servidor el tallafocs es tornarà a iniciar, impedint de nou que des de l'escriptori client ens hi puguem connectar. Per tal de desactivar-lo de manera definitiva (no recomanable) podem executar `sudo systemctl disable --now ufw```

Si tornem a anar al navegador i refresquem, hauríem de poder veure ara la plana web per defecte del servidor `nginx`.

## Connexió al servidor localment

Una altra prestació que ens proporciona IsardVDI és la possiblitat d'establir una connexió VPN d'usuari cap als escriptoris propis a IsardVDI (també cap a altres sota demanda).

El primer que haurem de fer és tornar a editar l'escriptori, en aquest cas només cal del servidor, i afegir la xarxa Wireguard. Aquesta nova interfície de xarxa ens apareixerà quan tornem a iniciar l'escriptori servidor i també podrem veure la IP que se li ha assignat al web d'IsardVDI.

Per a establir una connexió VPN haurem de seguir el [manual vpn](https://isard.gitlab.io/isardvdi-docs/user/vpn.ca/):

1. Instal·lar l'aplicatiu client VPN Wireguard pel nostre sistema operatiu [https://www.wireguard.com/install/](https://www.wireguard.com/install/)
2. Descarregar-nos el nostre fitxer de VPN personal de l'apartat de perfil de l'usuari (a dalt a la dreta al nostre nom)
3. Afegir aquest fitxer al nostre client wireguard (a Linux l'hem de moure a /etc/wireguard/isard-vpn.conf i a windows )
4. Activar la connexió (`sudo wg-quick up isard-vpn` a Linux i a Windows clicar a `Activate`)

![](wireguard.png)

Una vegada tinguem la connexió activa hauríem de poder arribar des del navegador del nostre ordinador local fins al servidor web introduint l'adreça `http://10.10.10.1` al navegador. Qualsevol servei que tinguem al nostre escriptori servidor serà accessible ara des del nostre ordinador local.

Això ens permet simular un servidor al núvol sense necessitat de crear un escriptori virtual client.

# Apèndix

## Xarxes Open vSwitch a IsardVDI

Internament IsardVDI implementa Open vSwitch (OVS). Es tracta d'un programari que pot processar trames i paquets aprofitant les característiques de Linux. Això fa que permeti implementar qualsevol tipus de comportament de xarxa a la capa de commutació de Linux (bridges).

Podem veure els sistemes Open vSwitch com conmutadors (switch) de xarxa que podem programar-ne el comportament com vulguem. En permetre processar les trames que entren o surten de cada port (o inclús generar-ne de pròpies) es pot partir de les dades emmagatzemades a la BBDD d'IsardVDI i mitjançant API REST modificar en temps real la configuració de cada port virtual on està cada escriptori virtual, limitant els broadcast de xarxa entre els ports dels escriptoris que pertanyen al mateix usuari o identificant-ne les adreces MAC i reenviant només el tràfic entre les MAC dels escriptoris d'un mateix usuari, tal com faria una VLAN.

A la plataforma pilotfp d'IsardVDI hi ha un servei central on ens validem i veiem les nostres dades a la plana web i un seguit d'hipervisors (on es virtualitzen els escriptoris) que s'inicien o s'aturen segons la demanda d'escriptoris que hi ha en cada moment. Aquests hipervisors en iniciar estableixen automàticament comunicació amb un concentrador OVS mitjançant túnels `geneve` sobre VPN encriptada `wireguard`. Aquests enllaços troncals formen una estructura d'estrella entre el concentrador principal i els hipervisors. Des d'IsardVDI gestionem dinàmicament la configuració de tots aquests switch en funció dels usuaris i escriptoris que s'inicien, establint en cada moment les regles OVS que apliquen el comportament de xarxa desitjat entre cada port d'aquests switch.

[Open vSwitch](https://www.openvswitch.org/) es tracta d'una tecnologia de xarxa que permet també aprofundir i entendre millor el funcionament de les diferents capes OSI de manera pràctica.