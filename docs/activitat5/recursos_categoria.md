# Gestió de recursos de la categoria

![](activitat5.images/left_nav_bar.png){width="20%"}


## Objectius

Avaluar les diferents accions que es poden realitzar amb els recursos de la categoria com a gestor.


## Índex de continguts

- Escriptoris
- Plantilles
- Desplegaments
- Mitjans


## Metodologia

El curs es realitza a [PilotFP](https://pilotfp.gencat.isardvdi.com/login). L’alumne (docent gestor inscrit) és el centre de l’aprenentatge. Aquest aprenentatge es construirà a partir de la pràctica proposada, assolint les competències relacionades amb les metodologies actives mitjançant el feedback.


## Desenvolupament

### 1. Gestió d'escriptoris

![](activitat5.images/admin-desktops.png)

La vista **Desktops** permet realitzar accions sobre tots els escriptoris de la categoria. Recomanem fer un cop d'ull a la documentació oficial sobre aquest apartat per entendre com gestionar tota la informació mostrada: **[Administració - Desktops](https://isard.gitlab.io/isardvdi-docs/manager/desktops.ca/#escriptoris)**

1. Crear escriptoris en massa a un dels grups proporcionats a la formació per tal de crear-ne 5 amb el nom **Escriptori per bulk**.
    1. Filtrar la vista perquè només surtin aquests escriptoris nous (p.e., filtrar per nom del grup).
2. Editar aquests escriptoris en massa canviant-los la **memòria RAM a 6GB**.
3. Habilitar el visor directe per algun d'aquests i comprovar que com a gestor podem accedir als escriptoris.
4. Eliminar un d'aquests escriptoris mitjançant el botó **Delete** dels seus detalls
5. Eliminar dos d'aquests escriptoris mitjançant la opció **Delete** de les **Global actions**. Per tal de fer seleccions múltiples podrem, o bé utilitzar un filtre o bé clicar sobre la selecció de diversos. El missatge de confirmació ens indicarà sobre quins (o quants) escriptoris es realitzarà l'acció.
6. Arrenca un escriptori mitjançant el botó **Start** a la seva fila
7. Atura aquest escriptori mitjançant la opció **Toggle started/stopped state** de les **Global actions**
8. Posa aquest escriptori com a **Server** al botó dels seus detalls
    1. Comprova com s'arrenca automàticament
    2. Prova d'aturar-lo amb el botó **Stop** i comprova que es torna a arrencar sol

!!! Documentació
    [Com crear escriptoris en massa](https://isard.gitlab.io/isardvdi-docs/manager/desktops.ca/#creacio-en-bloc)

    [Com editar escriptoris en massa](https://isard.gitlab.io/isardvdi-docs/manager/desktops.ca/#edicio-en-bloc)

    [Global actions](https://isard.gitlab.io/isardvdi-docs/manager/desktops.ca/#accions-globals)


### 2. Gestió de plantilles

![](activitat5.images/admin-templates.png)

La vista **Templates** permet realitzar accions sobre totes les plantilles de la categoria. Recomanem fer un cop d'ull a la documentació oficial sobre aquest apartat per entendre com gestionar tota la informació mostrada: **[Administració - Templates](https://isard.gitlab.io/isardvdi-docs/manager/templates.ca/#plantilles)**

1. Torna a la vista de **Desktops** i crea una plantilla habilitada a partir d'un escriptori amb nom **Plantilla des d'admin 1** amb el botó ![](activitat5.images/admin-template_it.png){width="10%"}
3. Vés a la vista **Templates** d'**Administració** i duplica la plantilla **Plantilla des d'admin 1** a una nova habilitada amb nom **Plantilla des d'admin 2**. Recordem que ambdues plantilles faràn ús del mateix disc i que en eliminar duplicades no s'esborren (fins que la original no sigui esborrada).
2. Vés a la vista d'usuari amb el botó ![](activitat5.images/admin-home.png){width="15%"} 
    1. Fes dos desplegaments amb noms **Desplegament 1** i **Desplegament 2** amb les plantilles **Plantilla des d'admin 1** i **Plantilla des d'admin 2** respectivament
4. Torna a la vista **Templates** d'**Administració** i esborra la plantilla **Plantilla des d'admin 1** i comprova com s'esborren els escriptoris del desplegament
5. Modifica la plantilla **Plantilla des d'admin 2** i canvia-li el nom a **Plantilla definitiva**

Aquesta pràctica està pensada per veure com l'usuari gestor pot crear desplegaments i escriptoris cap a diferents usuaris i grups sense necessitat de compartir la plantilla amb ningú.

També està pensada perquè el docent vegi la importància d'esborrar una plantilla amb escriptoris i desplegaments derivats, on s'esborrarien tots aquests items un rere l'altre, i com, en canvi, les plantilles duplicades poden "salvar-nos" de més d'un possible error perquè les seves cadenes de discos no estan lligades.

!!! Danger
    Recordem el perill i la importància d'esborrar una plantilla perquè es poden esborrar les dades i el treball dels usuaris de la categoria.

!!! Documentació
    [Com duplicar una plantilla](https://isard.gitlab.io/isardvdi-docs/manager/templates.ca/#duplicar)

    [Com crear un desplegament](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#crear-un-desplegament)

    [Com esborrar una plantilla](https://isard.gitlab.io/isardvdi-docs/manager/templates.ca/#esborrar)


### 3. Gestió de desplegaments

![](activitat5.images/admin-deployments.png)

A la vista **Deployments** es poden veure tots els desplegaments de la nostra categoria, informació sobre els escriptoris i maquinari virtual d'aquests. També podrem esborrar individualment o en massa.

1. Prova de crear alguns desplegaments qualsevols (des de la vista d'usuari) per tal de poder-hi realitzar les accions.
2. Esborra alguns desplegaments de forma individual (des de la vista de desplegaments de gestor).
3. Esborra alguns desplegaments en massa.

!!! Documentació
    [Desplegaments](https://isard.gitlab.io/isardvdi-docs/manager/deployments.ca/#desplegaments)


### 5. Gestió de mitjans

![](activitat5.images/admin-media.png)

A la vista **Media** es poden veure tots els mitjans de la nostra categoria. Entre la informació útil que hi trobarem hi ha també els escriptoris i plantilles que els tenen assignats.

1. Crea un nou mitjà des d'aquesta vista amb qualsevol nom des del botó ![](activitat5.images/admin-media-upload.png){width="15%"}. El sistema descarregarà des d'una URL un mitjà tipus ISO. Podeu fer servir un sistema mínim gràfic com (https://distro.ibiblio.org/damnsmall/current/current.iso)[https://distro.ibiblio.org/damnsmall/current/current.iso].
2. Espera a què es descarregui per complet i crea un escriptori amb nom **Desktop des de mitjà** a partir d'aquest mitjà amb el botó ![](activitat5.images/admin-media-desktop.png)
3. Quan s'hagi creat, esborra aquest mitjà. Comprova com l'escriptori nou no s'ha esborrat a la vista **Desktops** i només s'ha deslligat el seu mitjà (obre el botó de detalls de l'escriptori i comprova com no surt res a la taula **Media** d'aquest, o edita'l i comprova com a l'apartat **Media** del formulari d'edició no hi ha res afegit).
4. Crea un nou mitjà amb nom **Mitjà des d'admin** i afegeix-lo a l'escriptori **Desktop des de mitjà** (edita'l des de la vista **Desktops** i escriu el nom del mitjà a l'apartat **Media** del formulari d'edició).

!!! Documentació 
    [Com editar un escriptori](https://isard.gitlab.io/isardvdi-docs/manager/desktops.ca/#editar)


## Avaluació

S'avaluarà la realització de l'enquesta sobre els continguts d'aquest exercici.
